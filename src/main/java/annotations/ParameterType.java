/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package annotations;

public enum ParameterType {
	LOAD,
	LOAD_SHORT,
	LOAD_BYTE,
	RETURN,
	RETURN_SHORT,
	RETURN_BYTE,
	STORE,
	;

	public boolean isLoad() {
		return switch (this) {
			case LOAD, LOAD_SHORT, LOAD_BYTE -> true;
			default -> false;
		};
	}

	public boolean isReturn() {
		return switch (this) {
			case RETURN, RETURN_SHORT, RETURN_BYTE -> true;
			default -> false;
		};
	}

	public String asParameter() {
		return switch (this) {
			case LOAD, STORE, RETURN -> "int";
			case LOAD_SHORT, RETURN_SHORT -> "short";
			case LOAD_BYTE, RETURN_BYTE -> "byte";
		};
	}

	public String asFilename() {
		return switch (this) {
			case LOAD -> "Int";
			case LOAD_SHORT -> "Short";
			case LOAD_BYTE -> "Byte";
			case STORE -> "Store";
			default -> "";
		};
	}

	public String asReturnType() {
		return switch (this) {
			case RETURN -> "int";
			case RETURN_SHORT -> "short";
			case RETURN_BYTE -> "byte";
			case STORE -> "void";
			default -> throw new IllegalArgumentException();
		};
	}
}
