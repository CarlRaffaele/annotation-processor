/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package annotations;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

import static annotations.ParameterType.STORE;

@SupportedSourceVersion(SourceVersion.RELEASE_15)
@SupportedAnnotationTypes({"Annotations/annotations.VMFunction", "VMFunction", "annotations.VMFunction",
		"Annotations/annotations.VMFunctions", "VMFunctions", "annotations.VMFunctions",})
public class OpcodeProcessor extends AbstractProcessor {
	public static final int MAX_JLUXE_OPCODE = 0x1C9;
	private Messager messager;
	private Map<Integer, String> ops;

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		messager = processingEnv.getMessager();
		messager.printMessage(Diagnostic.Kind.NOTE, "Initializing processor");
		ops = new HashMap<>(MAX_JLUXE_OPCODE + 1, 1);
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		messager.printMessage(Diagnostic.Kind.NOTE, "Processing: " + annotations);
		for (TypeElement annotation : annotations) {
			Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
			List<ExecutableElement> annotatedMethods = annotatedElements.stream()
																		.filter(element -> element.getKind() == ElementKind.METHOD)
																		.map(element -> (ExecutableElement) element)
																		.collect(Collectors.toList());

			for (ExecutableElement annotatedMethod : annotatedMethods) {
				for (VMFunction ann : annotatedMethod.getAnnotationsByType(VMFunction.class)) {
					if (expectedParameterCount(ann) != annotatedMethod.getParameters().size()) {
						messager.printMessage(Diagnostic.Kind.ERROR, "Function parameters don't match expected count " +
								"for function: " + annotatedMethod.getSimpleName());
					}
					try {
						String interfaceName = writeInterface(annotatedMethod, ann);
						String instructionName = writeInstruction(ann, interfaceName);
						String opcodeName = writeOpcode(ann, interfaceName, instructionName);
						ops.put(ann.code(), "OPS[0x%X] = new %s(\"%s\", %s::%s, %b);".formatted(
								ann.code(),
								opcodeName,
								annotatedMethod.getSimpleName(),
								annotatedMethod.getEnclosingElement().getSimpleName(),
								annotatedMethod.getSimpleName(),
								ann.pure()));
					} catch (IOException e) {
						messager.printMessage(Diagnostic.Kind.WARNING, e.toString());
					} catch (Exception e) {
						messager.printMessage(Diagnostic.Kind.ERROR, e.toString());
					}
				}
			}
		}

		if (roundEnv.processingOver()) {
			List<String> sortedOps = ops.keySet()
										.stream()
										.sorted()
										.map(ops::get)
										.collect(Collectors.toList());
			try {
				writeGluxOpcode(MAX_JLUXE_OPCODE, sortedOps);
			} catch (IOException e) {
				messager.printMessage(Diagnostic.Kind.ERROR, e.getMessage());
			}
		}

		return true;
	}

	private int expectedParameterCount(VMFunction vmFunction) {
		int total = 0;
		for (ParameterType type : vmFunction.types()) {
			total += switch (type) {
				default -> 1;
				case STORE -> 2;
				case RETURN, RETURN_SHORT, RETURN_BYTE -> 0;
			};
		}
		return total;
	}

	private String writeInterface(ExecutableElement method, VMFunction annotation) throws IOException {
		TypeMirror returnType = method.getReturnType();
		long numStore = Arrays.stream(annotation.types()).filter(STORE::equals).count();
		boolean containsParameters = Arrays.stream(annotation.types()).anyMatch(pt -> pt.isLoad() || STORE.equals(pt));
		final String fileName;
		if (returnType.getKind() == TypeKind.INT && numStore == 0) {
			fileName = Arrays.stream(annotation.types())
							 .map(ParameterType::asFilename)
							 .reduce("", String::concat) + "Operator";
		} else if (returnType.getKind() == TypeKind.VOID) {
			fileName = Arrays.stream(annotation.types())
							 .map(ParameterType::asFilename)
							 .reduce("", String::concat) + "Consumer";
		} else {
			throw new IllegalArgumentException("Unknown returnType: " + returnType);
		}

		JavaFileObject file;
		try {
			file = processingEnv.getFiler().createSourceFile("Jluxe/jluxe.parser.instructions." + fileName);
		} catch (FilerException e) {
			// Source file already exists
			return fileName;
		}

		String parameters;
		if (containsParameters) {
			StringBuilder loads = new StringBuilder();
			StringBuilder stores = new StringBuilder();
			for (int i = 0, numLoads = 0, numStores = 0; i < annotation.types().length; i++) {
				switch (annotation.types()[i]) {
					case LOAD, LOAD_SHORT, LOAD_BYTE -> loads.append("%s v%d, ".formatted(annotation.types()[i].asParameter(), numLoads++));
					case STORE -> stores.append("LoadStoreMode store%d, int storeAddress%d, ".formatted(numStores,
							numStores++));
				}
			}
			loads.append(stores);
			parameters = loads.delete(loads.length() - 2, Integer.MAX_VALUE).toString();
		} else {
			parameters = "";
		}

		try (PrintWriter out = new PrintWriter(file.openWriter())) {
			out.write("""
					package jluxe.parser.instructions;
										
					%s
										
					@FunctionalInterface
					interface %s {
						%s accept(%s);
					}""".formatted(
					numStore != 0 ? "import jluxe.executor.LoadStoreMode;" : "",
					fileName,
					returnType.getKind() == TypeKind.INT ? "int" : "void",
					parameters
			));
		}

		return fileName;
	}

	private static String generateInstructionFields(ParameterType[] types, String interfaceFileName) {
		int numLoads = (int) Arrays.stream(types).filter(ParameterType::isLoad).count();
		int numStores = (int) Arrays.stream(types).filter(STORE::equals).count();
		boolean returns = Arrays.stream(types).anyMatch(ParameterType::isReturn);
		StringBuilder toAdd = new StringBuilder();

		toAdd.append("private static final Logger LOG = LogManager.getLogger();\n");
		if (numLoads > 0) {
			toAdd.append("private final LoadStoreMode l1");
			for (int i = 2; i <= numLoads; i++) {
				toAdd.append(", l%d".formatted(i));
			}
			toAdd.append(";\n");

			toAdd.append("private final int a1");
			for (int i = 2; i <= numLoads; i++) {
				toAdd.append(", a%d".formatted(i));
			}
			toAdd.append(";\n");
		}

		for (int i = 1; i <= numStores; i++) {
			toAdd.append("private final LoadStoreMode sm%d;\n".formatted(i));
			toAdd.append("private final int sa%d;\n".formatted(i));
		}

		if (returns) {
			toAdd.append("private final LoadStoreMode r;\n");
			toAdd.append("private final int r1;\n");
		}

		toAdd.append("private final int nextInst;\n");
		toAdd.append("private final ");
		toAdd.append(interfaceFileName);
		toAdd.append(" executor;\n");

		toAdd.append("private final boolean isConstant;\nprivate final int constantResult;\n");
		return toAdd.toString();
	}

	private static String generateInstructionConstructor(String className, String interfaceFilename,
														 ParameterType[] types) {
		StringBuilder toAdd = new StringBuilder();
		boolean hasReturn = false;

		//parameters
		toAdd.append("%s(boolean isPure, MemorySegment gameData, %s executor".formatted(className, interfaceFilename));
		int currentLoad = 1, currentStore = 1;
		for (ParameterType type : types) {
			toAdd.append(switch (type) {
				case LOAD, LOAD_BYTE, LOAD_SHORT -> ", LoadStoreMode l%d".formatted(currentLoad++);
				case STORE -> ", LoadStoreMode sm%d".formatted(currentStore++);
				case RETURN, RETURN_SHORT, RETURN_BYTE -> {
					hasReturn = true;
					yield ", LoadStoreMode r";
				}
			});
		}
		toAdd.append(") {\n");

		//body
		currentStore = currentLoad = 1;
		for (ParameterType type : types) {
			toAdd.append(switch (type) {
				case LOAD, LOAD_BYTE, LOAD_SHORT -> "this.l%1$d = l%1$d;\nthis.a%1$d = l%1$d.getLoadArgument(gameData);\n"
						.formatted(currentLoad++);
				case STORE -> "this.sm%1$d = sm%1$d;\nsa%1$d = sm%1$d.getStoreArgument(gameData);\n".formatted(currentStore++);
				case RETURN, RETURN_SHORT, RETURN_BYTE -> "this.r = r;\nr1 = r.getStoreArgument(gameData);\n";
			});
		}

		if (hasReturn && currentLoad > 1) {
			toAdd.append("this.isConstant = isPure && Instruction.isConstant(this.l1");
			for (int i = 2; i < currentLoad; i++) {
				toAdd.append(", this.l%1$d".formatted(i));
			}
			toAdd.append(");\n");
			toAdd.append("""
					if(isPure && this.isConstant){
						LOG.trace("Found constant instruction. Next inst address: " + gameData.position());
						this.constantResult = executor.accept(%s);
					} else {
						this.constantResult = -1;
					}
					""".formatted(generateInstructionExecutorBody(types)));
		} else {
			toAdd.append("this.isConstant = false;\nthis.constantResult = -1;\n");
		}

		toAdd.append("this.nextInst = gameData.position();\n this.executor = executor;\n}");
		return toAdd.toString();
	}

	private static StringBuilder generateInstructionExecutorBody(ParameterType[] types) {
		StringBuilder ret = new StringBuilder();
		int currentLoad = 1, numStore = 0;
		for (ParameterType pt : types) {
			switch (pt) {
				case LOAD -> ret.append("l%1$d.loadAsInt(a%1$d), ".formatted(currentLoad++));
				case LOAD_SHORT -> ret.append("l%1$d.loadAsShort(a%1$d), ".formatted(currentLoad++));
				case LOAD_BYTE -> ret.append("l%1$d.loadAsByte(a%1$d), ".formatted(currentLoad++));
				case STORE -> numStore++;
			}
		}

		for (int i = 1; i <= numStore; i++) {
			ret.append("sm%1$d, sa%1$d, ".formatted(i));
		}

		if (currentLoad != 1 || numStore > 0) {
			//remove the extra comma and space
			ret.delete(ret.length() - 2, Integer.MAX_VALUE);
		}

		return ret;
	}

	private static String generateInstructionRunBody(ParameterType[] types) {
		StringBuilder nonConstBody = new StringBuilder();
		String returnString = Arrays.stream(types)
									.filter(ParameterType::isReturn)
									.findAny()
									.map(rt -> switch (rt) {
										case RETURN -> "r.storeAsInt(r1, ";
										case RETURN_SHORT -> "r.storeAsShort(r1, ";
										case RETURN_BYTE -> "r.storeAsByte(r1, ";
										default -> throw new IllegalStateException("Non-return value marked as return type");
									})
									.orElse("");

		nonConstBody.append(returnString);

		nonConstBody.append("executor.accept(");
		nonConstBody.append(generateInstructionExecutorBody(types));
		if (!returnString.isEmpty()) {
			nonConstBody.append(")");
		}

		nonConstBody.append(");");
		return returnString.isEmpty() ?
				nonConstBody.toString() :
				"""
						if(this.isConstant){
							%1$sthis.constantResult);
							return;
						}
						%2$s""".formatted(returnString, nonConstBody.toString());
	}


	private String writeInstruction(VMFunction annotation, String interfaceFileName) throws IOException {
		String fileName = "GeneratedInstruction" + generateLSAbbreviations(annotation.types());
		JavaFileObject file;
		try {
			file = processingEnv.getFiler().createSourceFile("Jluxe/jluxe.parser.instructions." + fileName);
		} catch (FilerException e) {
			// Source file already exists
			return fileName;
		}

		try (PrintWriter out = new PrintWriter(file.openWriter())) {
			out.write("""
					package jluxe.parser.instructions;
										
					import gameMemory.MemorySegment;
					import jluxe.executor.LoadStoreMode;
					import org.apache.logging.log4j.LogManager;
					import org.apache.logging.log4j.Logger;
										
					class %s implements Instruction{
						%s
						
						%s
						
						@Override
						public void run(){
							%s
						}
						@Override
						public int nextInstructionAddress(){return nextInst;}
					}""".formatted(fileName,
					generateInstructionFields(annotation.types(), interfaceFileName),
					generateInstructionConstructor(fileName, interfaceFileName, annotation.types()),
					generateInstructionRunBody(annotation.types())));
		}

		return fileName;
	}

	private static String generateLSAbbreviations(ParameterType[] types) {
		return Arrays.stream(types)
					 .map(Enum::toString)
					 .reduce("", (a, b) -> a + b);
	}

	private void writeGluxOpcode(int numOpcodes, List<String> codes) throws IOException {
		JavaFileObject file = processingEnv.getFiler().createSourceFile("Jluxe/jluxe.parser.instructions" +
				".GeneratedOpcodes");

		try (PrintWriter out = new PrintWriter(file.openWriter())) {
			out.write("""		     
					package jluxe.parser.instructions;
					     
					import jluxe.executor.VMFunctions;
					import jluxe.parser.Parser;
					     
					public final class GeneratedOpcodes {
						public static final Opcode[] OPS = new Opcode[%d + 1];
					     
						static {
							%s
					    }
					}
					""".formatted(numOpcodes, String.join("\n", codes)));
		}
	}

	private String writeOpcode(VMFunction annotation, String interfaceFileName, String instructionFileName) throws IOException {
		String fileName = "GeneratedOpcode" + generateLSAbbreviations(annotation.types());
		JavaFileObject file;
		try {
			file = processingEnv.getFiler().createSourceFile("Jluxe/jluxe.parser.instructions." + fileName);
		} catch (FilerException e) {
			// Source file already exists
			return fileName;
		}

		String argMode;
		if (annotation.types().length > 6) {
			argMode = "int argModes = gameData.getInt();";
		} else if (annotation.types().length > 4) {
			argMode = "int argModes = (gameData.getByte() << 16) | (gameData.getShort() & 0xffff);";
		} else if (annotation.types().length > 2) {
			argMode = "short argModes = gameData.getShort();";
		} else if (annotation.types().length > 0) {
			argMode = "byte argModes = gameData.getByte();";
		} else {
			argMode = "";
		}

		StringBuilder parseBody = new StringBuilder();
		if (annotation.types().length > 0) {
			parseBody.append(", ");
			if ((annotation.types().length & 1) == 0) {
				switch (annotation.types().length) {
					case 8:
						parseBody.append("LoadStoreMode.fromID((argModes >> 24) & 0xf), LoadStoreMode.fromID((argModes >> 28) & 0xf),");
					case 6:
						parseBody.append("LoadStoreMode.fromID((argModes >> 16) & 0xf), LoadStoreMode.fromID((argModes >> 20) & 0xf),");
					case 4:
						parseBody.append("LoadStoreMode.fromID((argModes >> 8) & 0xf), LoadStoreMode.fromID((argModes >> 12) & 0xf),");
					case 2:
						parseBody.append("LoadStoreMode.fromID(argModes & 0xf), LoadStoreMode.fromID((argModes >> 4) & 0xf)");
				}
			} else {
				switch (annotation.types().length & 0xfffffffe) {
					case 6:
						parseBody.append("LoadStoreMode.fromID((argModes >> 24) & 0xf), LoadStoreMode.fromID((argModes >> 28) & 0xf),");
					case 4:
						parseBody.append("LoadStoreMode.fromID((argModes >> 16) & 0xf), LoadStoreMode.fromID((argModes >> 20) & 0xf),");
					case 2:
						parseBody.append("LoadStoreMode.fromID((argModes >> 8) & 0xf), LoadStoreMode.fromID((argModes >> 12) & 0xf),");
				}
				parseBody.append("LoadStoreMode.fromID(argModes & 0xf)");
			}
		}

		try (PrintWriter out = new PrintWriter(file.openWriter())) {
			out.write("""
					package jluxe.parser.instructions;

					import gameMemory.MemorySegment;
					import jluxe.executor.LoadStoreMode;

					public class %1$s extends GluxOpcode {
						private final %2$s executor;
						
						%1$s(String name, %2$s executor, boolean pureFunction){
							super(name, pureFunction);
					  		this.executor = executor;
						}
						
						@Override
					 	public Instruction parse(MemorySegment gameData) {
					 		%4$s
					 		return new %3$s(super.pureFunction, gameData, executor%5$s);
					 	}
					}
					""".formatted(fileName, interfaceFileName, instructionFileName, argMode, parseBody.toString()));
		}
		return fileName;
	}
}
